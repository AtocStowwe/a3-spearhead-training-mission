params [ "_location", "_damarr", "_itr", "_terminal", "_multiUnit"];
/*
	PARAMETERS
	_location - A prop in the world, preferibly a decal like a helipad, to spawn
		the unit on.
	_damarr - The array of potential damage amounts to inflict on the unit. Each
		number should be between 0 and 1. Higher is more damaging.
	_itr - How many damage passes to do on the unit. More is harder.
	_terminal - The terminal that the unit is being spawned from.
*/

//delete the old unit tied to this terminal
if(isNil {_multiUnit}) then {
	deleteVehicle (_terminal getVariable ["medUnit", nil]);
};

//animate the terminal opening and rest for its animation
[_terminal ,3] call BIS_fnc_dataTerminalAnimate;
uisleep 5;

//preinit for unit
_group = createGroup west;
_unit = _group createUnit ["b_survivor_F", getpos _location, [], 0, "FORM"];

//disable AI and remove everything
dostop _unit;
removeallweapons _unit;
removeallassigneditems _unit;
_unit disableAI "MOVE";

//set the unit's ref in the terminal
if(isNil {_multiUnit}) then {
	_terminal setVariable ["medUnit", _unit];
};

_unit addAction [
	"Delete Unit",
	{
		deleteVehicle (_this select 0);
	},
	_x,
	100,
	true,
	true,
	"",
	"true",
	5
];
for "_i" from 0 to _itr do {
	[_unit, selectRandom _damarr, "leg_r", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
	[_unit, selectRandom _damarr, "leg_l", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
	[_unit, selectRandom _damarr, "body", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
	[_unit, selectRandom _damarr, "head", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
	[_unit, selectRandom _damarr, "hand_r", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
	[_unit, selectRandom _damarr, "hand_l", selectrandom ["stab","bullet","falling"]] call ace_medical_fnc_addDamageToUnit;
};

[_terminal, 0] call BIS_fnc_dataTerminalAnimate;
hint "Patient is ready!";
